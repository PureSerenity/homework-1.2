package com.vadymp;

public class Main {

    public static void main(String[] args) {
        InputManager inputManager = new InputManager();
        PointList pointList = inputManager.getPoints();
        Circle circle = inputManager.getCircle();
        inputManager.showInfo(pointList, circle);
        System.out.println("=====ПРОГРАММА ЗАВЕРШЕНА!=====");
    }
}
